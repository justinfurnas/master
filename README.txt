
CONTENTS OF THIS FILE
---------------------

 * About PHPWow
 * Configuration and features
 * Contributing
 * More information

ABOUT PHPWow
------------

PHPWow is an open source PHP library that communicates directly with the Blizzard
World of Warcraft Community API. It can be used as a gateway for connecting your
website or mobile web application to the Blizzard API for retrieving any data
available through the API.

Legal information about PHPWow:
 * Know your rights when using PHPWow:
   See LICENSE.txt

CONFIGURATION AND FEATURES
--------------------------

PHPWow is easy to configure and use. See the INSTALL.txt file for in-depth
documentation on installing this software. 

Configurations options:

  * $config['api_key'] - Required to access the Blizzard API. You must create
    an account on Mashery (https://dev.battle.net/) to get an API key.

  * $config['region'] - Used to specify what region of the API you want to access.
    Default: us
    
  * $config['locale'] - Used to specify the locality of the results.
    Default: en_US

CONTRIBUTING
------------

If you would like to contribute to the PHPWow project, please create a fork and
make a pull request. Our team will review the request and merge it into our project.

MORE INFORMATION
----------------

 * See the PHPWow online documentation:
   - Documentation is a work in progress -