<?php

define("INVALID_CHARACTER","Invalid Character");

define("RACE_HUMAN","Human");
define("RACE_ORC","Orc");
define("RACE_DWARF","Dwarf");
define("RACE_NIGHTELF","Night Elf");
define("RACE_UNDEAD","Forsaken");
define("RACE_TAUREN","Tauren");
define("RACE_GNOME","Gnome");
define("RACE_TROLL","Troll");
define("RACE_GOBLIN","Goblin");
define("RACE_BLOODELF","Blood Elf");
define("RACE_DRAENEI","Draenei");
define("RACE_WORGEN","Worgen");
define("RACE_PANDAREN_NEUTRAL","Pandarian");
define("RACE_PANDAREN_ALLIANCE","Pandarian");
define("RACE_PANDAREN_HORDE","Pandarian");
define("RACE_NIGHTBORNE","Nightborne");
define("RACE_HIGHMOUNTAIN_TAUREN","Highmountain Tauren");
define("RACE_VOIDELF","Void Elf");
define("RACE_LIGHTFORGED_DRAENEI","Lightforged Draenei");
define("RACE_ZANDALARI_TROLL","Zandalari Troll");
define("RACE_KUL_TIRAN","Kul Tiran");
define("RACE_DARK_IRON_DWARF","Dark Iron Dwarf");
define("RACE_MAGHAR_ORC","Mag'har Orc");

$races = array(
	1 => RACE_HUMAN,
	2 => RACE_ORC,
	3 => RACE_DWARF,
	4 => RACE_NIGHTELF,
	5 => RACE_UNDEAD,
	6 => RACE_TAUREN,
	7 => RACE_GNOME,
	8 => RACE_TROLL,
	9 => RACE_GOBLIN,
	10 => RACE_BLOODELF,
	11 => RACE_DRAENEI,
	22 => RACE_WORGEN,
	24 => RACE_PANDAREN_NEUTRAL,
	25 => RACE_PANDAREN_ALLIANCE,
	26 => RACE_PANDAREN_HORDE,
	27 => RACE_NIGHTBORNE,
	28 => RACE_HIGHMOUNTAIN_TAUREN,
	29 => RACE_VOIDELF,
	30 => RACE_LIGHTFORGED_DRAENEI,
	31 => RACE_ZANDALARI_TROLL,
	32 => RACE_KUL_TIRAN,
	34 => RACE_DARK_IRON_DWARF,
	36 => RACE_MAGHAR_ORC,
);

define("WARRIOR","Warrior");
define("PALADIN","Paladin");
define("HUNTER","Hunter");
define("ROGUE","Rogue");
define("PRIEST","Priest");
define("DEATH_KNIGHT","Death Knight");
define("SHAMAN","Shaman");
define("MAGE","Mage");
define("WARLOCK","Warlock");
define("MONK","Monk");
define("DRUID","Druid");
define("DEMON_HUNTER","Demon Hunter");

$classes = array(
	1 => WARRIOR,
	2 => PALADIN,
	3 => HUNTER,
	4 => ROGUE,
	5 => PRIEST,
	6 => DEATH_KNIGHT,
	7 => SHAMAN,
	8 => MAGE,
	9 => WARLOCK,
	10 => MONK,
	11 => DRUID,
	12 => DEMON_HUNTER,
);

