<?php

require_once('config.php');
require_once("defines.inc.php");

class Character {

	public $api_key = '';
	public $realm = 'test-realm';
	public $charactername = 'Test';

	public function __construct($character,$realm) {
		global $races, $classes, $config;
		$this->api_key = $config['api_key'];
		$this->locale = $config['locale'];
		$this->races = $races;
		$this->classes = $classes;

		$this->charactername = $character;
		$this->realm = $realm;
	}

	public function fetch($fields) {
		$url = 'https://us.api.battle.net/wow/character/'.
		$this->realm .'/'.
		$this->charactername .'?fields='.
		$fields .'&locale='.
		$this->locale .'&apikey='.
		$this->api_key;

		$ch = curl_init($url);
		$options = array(
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_HEADER => FALSE,
		);

		curl_setopt_array($ch, $options);
		$result = json_decode(curl_exec($ch));
		curl_close($ch);

		return $result;
	}

	public function getSpecialization() {
		$result = $this->fetch('talents');

		if (!empty($result->talents)) {
			foreach ($result->talents as $talent) {
				if (isset($talent->selected)) {
					return $talent->spec->name;
				}
			}
		}
		else {
			return INVALID_CHARACTER;
		}
	}

	public function getRace() {
		$result = $this->fetch('profile');

		return $this->races[$result->race];
	}

	public function getClass() {
		$result = $this->fetch('profile');

		return $this->classes[$result->class];
	}

	public function getLevel() {
		$result = $this->fetch('profile');
		
		return $result->level;
	}

  public function getProfessions($type = "primary") {
  	$result = $this->fetch('professions');

  	switch ($type) {
  		case 'primary':
  		  return $result->professions->primary;
  		  break;
  		case 'secondary':
  		  return $result->professions->secondary;
  		  break;
  		default: 
  		  return $result->professions->primary;
  		  break;
  	}
  }

  public function getTalents($selected = FALSE) {
  	$result = $this->fetch('talents');
  	foreach ($result->talents as $spec) {
  		if ($selected) {
  			if ($spec->selected) {
  				$talents = $spec->talents;
  			}
  		}
  		else {
  			if (!empty($spec->talents)) {
  				foreach ($spec->talents as $talent) {
  					$spec = $talent->spec->name;

  					$talents[$talent->spec->name][] = $talent;

  				}
  			}
			}
  	}
  	return $talents;
	}

	public function getReputation($faction = 'all') {
		$result = $this->fetch('reputation');

		if ($faction == 'all') {
			$reputation = $result->reputation;
		}
		else {
			foreach ($result->reputation as $f) {
				if ($f->name == $faction) {
					$reputation = $f;
				}
			}
		}

		if (isset($reputation)) {
			return $reputation;
		}
		else {
			return NULL;
		}
	}

	public function getAvatar() {
		$result = $this->fetch('profile');

		return $result->thumbnail;
	}

	public function getGuild() {
		$result = $this->fetch('guild');

		return $result->guild->name;
	}
}